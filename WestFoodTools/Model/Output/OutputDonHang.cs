﻿using System.ComponentModel;

namespace WestFoodTools
{
    public class OutputDonHang
    {
        public string UniqueID { get; set; } = "";
        [Description("Mã đơn hàng đối chiếu")]
        public string madonhangdoichieu { get; set; } = "";
        [Description("Mã đơn hàng")]
        public string madonhang { get; set; } = "";
        [Description("Ngày đơn hàng")]
        public string ngaydonhang { get; set; } = "";
        [Description("Mã Nhóm")]
        public string manhom { get; set; } = "";
        [Description("Nhóm")]
        public string nhom { get; set; } = "";
        [Description("Mã sản phẩm")]
        public string masanpham { get; set; } = "";
        [Description("Tên sản phẩm")]
        public string tensanpham { get; set; } = "";
        [Description("Mã công đoạn")]
        public string macongdoan { get; set; } = "";
        [Description("Công đoạn")]
        public string congdoan { get; set; } = "";
        [Description("Đơn giá")]
        public string dongia { get; set; } = "";
        [Description("Số chứng từ")]
        public string sochungtu { get; set; } = "";
        [Description("Số lượng chứng từ")]
        public string slchungtu { get; set; } = "";
        [Description("Số lượng thực tế")]
        public string slthucte { get; set; } = "";
        [Description("Số lượng hao hụt")]
        public string slhaohut { get; set; } = "";
        [Description("Đơn vị")]
        public string donvi { get; set; } = "";
        [Description("Tình trạng")]
        public string tinhtrang { get; set; } = "";
        [Description("Tỷ lệ hoàn thành")]
        public string tylehoanthanh { get; set; } = "";
        [Description("Hệ số")]
        public string heso { get; set; } = "";
        [Description("Ghi chú")]
        public string ghichu { get; set; } = "";
        [Description("Tổng thời gian (dùng thống kê)")]
        public string tongthoigian { get; set; } = "";
        [Description("Tổng số lượng thực tế (dùng thống kê)")]
        public string tongslthucte { get; set; } = "";
        [Description("Thời gian phân bổ (dùng thống kê)")]
        public string tgphanbo { get; set; } = "";
    }
}
