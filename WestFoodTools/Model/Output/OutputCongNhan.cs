﻿using System.ComponentModel;

namespace WestFoodTools
{
    public class OutputCongNhan
    {
        public string UniqueID { get; set; } = "";
        [Description("Mã nhân viên")]
        public string manv { get; set; } = "";
        [Description("Tên nhân viên")]
        public string tennhanvien { get; set; } = "";
        [Description("Số rổ")]
        public string soro { get; set; } = "";
        [Description("Số lượng")]
        public string soluong { get; set; } = "";
        [Description("Thời gian")]
        public string thoigian { get; set; } = "";
        [Description("Mượn người")]
        public string muonnguoi { get; set; } = "";
        [Description("Ăn riêng")]
        public string anrieng { get; set; } = "";
        [Description("Ăn chung")]
        public string anchung { get; set; } = "";
        [Description("Mã đơn hàng")]
        public string madonhang { get; set; } = "";
        [Description("Mã đơn hàng đối chiếu")]
        public string madonhangdoichieu { get; set; } = "";
        [Description("Tổng thời gian ghi năng xuất (dùng thống kê)")]
        public string tongthoigianghinangsuat { get; set; } = "";
        [Description("Thời gian phân bổ (dùng thống kê)")]
        public string thoigianphanbo { get; set; } = "";
    }
}
