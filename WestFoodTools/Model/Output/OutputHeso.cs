﻿using System.ComponentModel;

namespace WestFoodTools
{
    public class OutputHeso
    {
        public string UniqueID { get; set; } = "";
        [Description("Tên hệ số")]
        public string tenheso { get; set; } = "";
        [Description("Mã hệ số")]
        public string heso { get; set; } = "";
        [Description("Mã đơn hàng")]
        public string madonhang { get; set; } = "";
        [Description("Mã đơn hàng đối chiếu")]
        public string madonhangdoichieu { get; set; } = "";
    }
}
