﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WestFoodTools
{
    public class InputDonHang
    {
        public string madonhang { get; set; } = "";
        public string ngaydonhang { get; set; } = "";
        public string manhom { get; set; } = "";
        public string nhom { get; set; } = "";
        public string macongdoan { get; set; } = "";
        public string congdoan { get; set; } = "";
        public string dongia { get; set; } = "";
        public string sochungtu { get; set; } = "";
        public string slchungtu { get; set; } = "";
        public string slthucte { get; set; } = "";
        public string slhaohut { get; set; } = "";
        public string donvi { get; set; } = "";
        public string tinhtrang { get; set; } = "";
        public string tylehoanthanh { get; set; } = "";
        public string heso { get; set; } = "";
        public string ghichu { get; set; } = "";
        public string tongthoigianchitiet { get; set; } = "";
    }
}
