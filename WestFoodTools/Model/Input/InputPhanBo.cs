﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WestFoodTools
{
    public class InputPhanBo
    {
        public string madonhang { get; set; } = "";
        public string masanpham { get; set; } = "";
        public string tensanpham { get; set; } = "";
        public string slchungtu { get; set; } = "";
        public string slthucte { get; set; } = "";
        public string slhaohut { get; set; } = "";
        public string thoigianphanbo { get; set; } = "";
    }
}
