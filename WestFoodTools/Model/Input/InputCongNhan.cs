﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WestFoodTools
{
    public class InputCongNhan
    {
        public string manv { get; set; } = "";
        public string tennhanvien { get; set; } = "";
        public string soro { get; set; } = "";
        public string soluong { get; set; } = "";
        public string thoigian { get; set; } = "";
        public string muonnguoi { get; set; } = "";
        public string anrieng { get; set; } = "";
        public string anchung { get; set; } = "";
        public string madonhang { get; set; } = "";
        public string thoigiannangsuat { get; set; } = "";
    }
}
