﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace WestFoodTools
{
    public partial class Frm_Excel_Order : Form
    {
        DataTable _tblSheet;
        DataTable _tblOrder;
        DataTable _tblWorker;
        DataTable _tblProtivity;
        private string _FileName = "";

        //-----------List Model input
        List<InputDonHang> _InputDonHang = new List<InputDonHang>();
        List<InputCongNhan> _InputCongNhan = new List<InputCongNhan>();
        List<InputPhanBo> _InputPhanBo = new List<InputPhanBo>();
        //-----------List Model output
        List<OutputDonHang> _OutputDonHang = new List<OutputDonHang>();
        List<OutputCongNhan> _OutputCongNhan = new List<OutputCongNhan>();
        List<OutputHeso> _OutputHeso = new List<OutputHeso>();
        //-----------List obj input, output
        List<InputObject> _ListInputObject = new List<InputObject>();
        List<OutputObject> _ListOutputObject = new List<OutputObject>();

        public Frm_Excel_Order()
        {
            InitializeComponent();
            //txt_File2.Text = Application.StartupPath + "\\Excel_DonHang.xlsx";
        }

        private void Frm_Excel_Order_Load(object sender, EventArgs e)
        {

        }

        private void btnOpen1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ODialog = new OpenFileDialog();
            ODialog.Filter = "Excel Files|*.txt;*";
            ODialog.Multiselect = false;
            if (DialogResult.OK == ODialog.ShowDialog())
            {
                txt_File1.Text = ODialog.FileName;
                _FileName = Path.GetFileNameWithoutExtension(ODialog.FileName).Trim();
            }
        }

        private void btnOpen2_Click(object sender, EventArgs e)
        {
            if (txt_File1.Text.Trim() != "")
            {
                SaveFileDialog FDialog = new SaveFileDialog();
                FDialog.Filter = "Excel Files|*.xlsx;*";
                FDialog.FileName = _FileName+"_ThayDoi";
                FDialog.ShowDialog();

                txt_File2.Text = FDialog.FileName;
            }
            else
            {
                MessageBox.Show("Chưa chọn tệp tin cần chuyển đổi!.");
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txt_File1.Text.Trim() == string.Empty || txt_File2.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Đường dẫn tập tin hoặc thư mục lưu tập tin không được trống !.");
                return;
            }

            btnExit.Enabled = false;
            btnOK.Enabled = false;

            string Mess = ReadExcel();
            Mess += ProcessExcel();
            Mess += CreateExcel();

            txt_Message.Text = Mess;

            btnExit.Enabled = true;
            btnOK.Enabled = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        string ProcessExcel()
        {
            string Message = "";
            Message += FilterInputObject();
            Message += ProcessInputObject();
            Message += FilterOutputObject();
            Message += RenameObject();
            Message += InitNewOutput();
            return Message;
        }
        string ReadExcel()
        {
            string zMessage = "";
            _tblSheet = new DataTable();
            _tblSheet = Data_Access.GetSheet(txt_File1.Text.Trim());
            if (_tblSheet.Rows.Count <= 0)
            {
                MessageBox.Show("Vui lòng đóng File trước khi upload  !");
                zMessage = "Failed";
            }
            else
            {
                _tblOrder = Data_Access.GetTable(txt_File1.Text.Trim(), _tblSheet.Rows[0]["sheetName"].ToString());
                _tblWorker = Data_Access.GetTable(txt_File1.Text.Trim(), _tblSheet.Rows[1]["sheetName"].ToString());
                _tblProtivity = Data_Access.GetTable(txt_File1.Text.Trim(), _tblSheet.Rows[2]["sheetName"].ToString());

                zMessage += CheckOrder();
                zMessage += CheckWorker();
                zMessage += CheckProtivity();

                if (zMessage == string.Empty)
                {
                    Create_ListOrder();
                    Create_ListWorker();
                    Create_ListProtivity();
                }
            }
            return zMessage;
        }
        string CreateExcel()
        {
            string Message = "";
            Message = Data_Access.ExportListToExcel(_OutputDonHang, _OutputCongNhan, _OutputHeso, txt_File2.Text);
            if (Message == "OK")
            {
                NewObject();
                Message = "Đã tạo tập tin thành công !." + Environment.NewLine + "Bạn có muốn mở thư mục chứa ?.";
                if (MessageBox.Show(Message, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Process.Start(txt_File2.Text);
                }
            }
            else
            {
                NewObject();
                return "Lỗi !. " + Message;
            }

            return Message;
        }

        void NewObject()
        {
            _tblSheet = new DataTable();
            _tblOrder = new DataTable();
            _tblWorker = new DataTable();
            _tblProtivity = new DataTable();

            _InputDonHang = new List<InputDonHang>();
            _InputCongNhan = new List<InputCongNhan>();
            _InputPhanBo = new List<InputPhanBo>();
            _OutputDonHang = new List<OutputDonHang>();
            _OutputCongNhan = new List<OutputCongNhan>();
            _OutputHeso = new List<OutputHeso>();
            _ListInputObject = new List<InputObject>();
            _ListOutputObject = new List<OutputObject>();
        }

        #region //  Check Data  //
        private string CheckOrder()
        {
            string zResoult = "";
            string zMessageRow = "";
            int No = 1;
            foreach (DataRow Row in _tblOrder.Rows)
            {
                No++;
                string zMessCol = "";
                for (int i = 1; i < _tblOrder.Columns.Count - 5; i++)
                {
                    string a = Row[i].ToString();
                    if (Row[i].ToString() == "")
                    {
                        
                        zMessCol += "Err";
                    }
                }
                if (zMessCol != "")
                {
                    zMessageRow += Environment.NewLine + "Đơn hàng Dòng: " + No + " có dữ liệu bị thiếu";
                }
            }
            zResoult += zMessageRow;
            return zResoult;
        }
        private string CheckWorker()
        {
            string zResoult = "";
            string zMessageRow = "";
            int No = 1;
            string zMessCol = "";
            foreach (DataRow Row in _tblWorker.Rows)
            {
                No++;
                zMessCol = "";
                if (Row[1].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (Row[2].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (Row[9].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (zMessCol != "")
                {
                    zMessageRow += Environment.NewLine + " Công nhân Dòng: " + No + " có dữ liệu bị thiếu";
                }
            }
            zResoult += zMessageRow;
            return zResoult;
        }
        private string CheckProtivity()
        {
            string zResoult = "";
            string zMessageRow = "";
            int No = 1;
            string zMessCol = "";
            foreach (DataRow Row in _tblProtivity.Rows)
            {
                No++;
                zMessCol = "";
                if (Row[1].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (Row[2].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (Row[3].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (Row[4].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (Row[6].ToString() == "")
                {
                    zMessCol += "ERR";
                }
                if (zMessCol != "")
                {
                    zMessageRow += Environment.NewLine + " Phân bổ Dòng: " + No + " có dữ liệu bị thiếu";
                }
            }
            zResoult += zMessageRow;
            return zResoult;
        }
        #endregion

        #region //  xử lý chuyển đổi data ra đúng format //
        private void Create_ListOrder()
        {
            _InputDonHang.Clear();
            for (int i = 0; i < _tblOrder.Rows.Count; i++)
            {
                InputDonHang zOrder = new InputDonHang();
                zOrder.madonhang = _tblOrder.Rows[i][1].ToString();
                zOrder.ngaydonhang = _tblOrder.Rows[i][2].ToString();
                zOrder.manhom = _tblOrder.Rows[i][3].ToString();
                zOrder.nhom = _tblOrder.Rows[i][4].ToString();
                zOrder.macongdoan = _tblOrder.Rows[i][5].ToString();
                zOrder.congdoan = _tblOrder.Rows[i][6].ToString();
                zOrder.dongia = _tblOrder.Rows[i][7].ToString();
                zOrder.sochungtu = _tblOrder.Rows[i][8].ToString();
                zOrder.slchungtu = _tblOrder.Rows[i][9].ToString();
                zOrder.slthucte = _tblOrder.Rows[i][10].ToString();
                zOrder.slhaohut = _tblOrder.Rows[i][11].ToString();
                zOrder.donvi = _tblOrder.Rows[i][12].ToString();
                zOrder.tinhtrang = _tblOrder.Rows[i][13].ToString();
                zOrder.tylehoanthanh = _tblOrder.Rows[i][14].ToString();
                zOrder.heso = _tblOrder.Rows[i][15].ToString();
                zOrder.ghichu = _tblOrder.Rows[i][16].ToString();
                zOrder.tongthoigianchitiet = _tblOrder.Rows[i][17].ToString();
                _InputDonHang.Add(zOrder);
            }
        }
        private void Create_ListWorker()
        {
            _InputCongNhan.Clear();
            for (int i = 0; i < _tblWorker.Rows.Count; i++)
            {
                InputCongNhan zWorker = new InputCongNhan();
                zWorker.manv = _tblWorker.Rows[i][1].ToString();
                zWorker.tennhanvien = _tblWorker.Rows[i][2].ToString();
                zWorker.soro = _tblWorker.Rows[i][3].ToString();
                zWorker.soluong = _tblWorker.Rows[i][4].ToString();
                zWorker.thoigian = _tblWorker.Rows[i][5].ToString();
                zWorker.muonnguoi = _tblWorker.Rows[i][6].ToString();
                zWorker.anrieng = _tblWorker.Rows[i][7].ToString();
                zWorker.anchung = _tblWorker.Rows[i][8].ToString();
                zWorker.madonhang = _tblWorker.Rows[i][9].ToString();
                _InputCongNhan.Add(zWorker);
            }
        }
        private void Create_ListProtivity()
        {
            _InputPhanBo.Clear();
            for (int i = 0; i < _tblProtivity.Rows.Count; i++)
            {
                InputPhanBo zProtivity = new InputPhanBo();
                zProtivity.masanpham = _tblProtivity.Rows[i][1].ToString();
                zProtivity.tensanpham = _tblProtivity.Rows[i][2].ToString();
                zProtivity.slchungtu = _tblProtivity.Rows[i][3].ToString();
                zProtivity.slthucte = _tblProtivity.Rows[i][4].ToString();
                zProtivity.slhaohut = _tblProtivity.Rows[i][5].ToString();
                zProtivity.madonhang = _tblProtivity.Rows[i][6].ToString();
                _InputPhanBo.Add(zProtivity);
            }
        }
        private string FilterInputObject()
        {
            try
            {
                foreach (InputDonHang DonHang in _InputDonHang)
                {
                    InputObject zObject = new InputObject();
                    zObject.madonhang = DonHang.madonhang;
                    zObject.ngaydonhang = DonHang.ngaydonhang;
                    zObject.manhom = DonHang.manhom;
                    zObject.nhom = DonHang.nhom;
                    zObject.macongdoan = DonHang.macongdoan;
                    zObject.congdoan = DonHang.congdoan;
                    zObject.dongia = DonHang.dongia;
                    zObject.sochungtu = DonHang.sochungtu;
                    zObject.slchungtu = DonHang.slchungtu;
                    zObject.slthucte = DonHang.slthucte;
                    zObject.slhaohut = DonHang.slhaohut;
                    zObject.donvi = DonHang.donvi;
                    zObject.tinhtrang = DonHang.tinhtrang;
                    zObject.tylehoanthanh = DonHang.tylehoanthanh;
                    zObject.ghichu = DonHang.ghichu;
                    zObject.heso = DonHang.heso;
                    zObject.tongthoigianchitiet = DonHang.tongthoigianchitiet;

                    zObject.ListCongNhan = _InputCongNhan.FindAll(x => x.madonhang == DonHang.madonhang);
                    zObject.ListPhanBo = _InputPhanBo.FindAll(x => x.madonhang == DonHang.madonhang);

                    _ListInputObject.Add(zObject);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            txt_Message.Text += "tổng xử lý đối tường " + _ListInputObject.Count;
            return string.Empty;
        }
        private string ProcessInputObject()
        {

            for (int i = 0; i < _ListInputObject.Count; i++)
            {
                InputObject zObj = _ListInputObject[i];
                XuLyListDonHang(zObj);
            }


            return string.Empty;
        }

        string XuLyListDonHang(InputObject InObject)
        {
            string Message = "";
            try
            {
                List<InputPhanBo> ListPhanBo = InObject.ListPhanBo;
                foreach (InputPhanBo Pb in ListPhanBo)
                {
                    string RandomID = Guid.NewGuid().ToString();

                    #region Obj1 DonHang
                    OutputDonHang zOutDonHang = new OutputDonHang();
                    zOutDonHang.UniqueID = RandomID;
                    zOutDonHang.madonhangdoichieu = InObject.madonhang.Trim();
                    zOutDonHang.madonhang = InObject.madonhang.Trim();
                    zOutDonHang.ngaydonhang = InObject.ngaydonhang.Trim();
                    zOutDonHang.manhom = InObject.manhom.Trim();
                    zOutDonHang.nhom = InObject.nhom.Trim();
                    zOutDonHang.masanpham = Pb.masanpham.Trim();
                    zOutDonHang.tensanpham = Pb.tensanpham.Trim();
                    zOutDonHang.macongdoan = InObject.macongdoan.Trim();
                    zOutDonHang.congdoan = InObject.congdoan.Trim();
                    zOutDonHang.dongia = InObject.dongia.Trim();
                    zOutDonHang.sochungtu = InObject.sochungtu.Trim();
                    zOutDonHang.slchungtu = Pb.slchungtu.Trim();
                    zOutDonHang.slthucte = Pb.slthucte.Trim();
                    zOutDonHang.slhaohut = Pb.slhaohut.Trim();
                    zOutDonHang.donvi = InObject.donvi.Trim();
                    zOutDonHang.tinhtrang = InObject.tinhtrang.Trim();
                    zOutDonHang.tylehoanthanh = InObject.tylehoanthanh.Trim();
                    zOutDonHang.ghichu = InObject.ghichu.Trim();

                    //*******************************************************************//

                    if (InObject.tongthoigianchitiet != string.Empty)
                    {
                        zOutDonHang.tongthoigian = InObject.tongthoigianchitiet;
                        zOutDonHang.tongslthucte = InObject.slthucte;
                        zOutDonHang.tgphanbo = (float.Parse(InObject.tongthoigianchitiet) / float.Parse(InObject.slthucte) * float.Parse(Pb.slthucte)).ToString();
                    }

                    //*******************************************************************//
                    _OutputDonHang.Add(zOutDonHang);
                    #endregion

                    #region Obj2 HeSo
                    XuLyListHeSo(RandomID, InObject);
                    #endregion

                    #region Obj3 CongNhan       
                    XuLyListCongNhan(RandomID, InObject, zOutDonHang.tgphanbo);
                    #endregion
                }
            }
            catch (Exception Ex)
            {
                Message = Ex.ToString();
            }
            return Message;
        }

        string XuLyListHeSo(string UniqueID, InputObject InObject)
        {
            string MaDonHang = InObject.madonhang;
            string Heso = InObject.heso;
            string Message = "";
            try
            {
                if (Heso.Contains(","))
                {
                    string[] temp = Heso.Split(',');
                    foreach (string s in temp)
                    {
                        OutputHeso zOutHeSo = new OutputHeso();
                        zOutHeSo.UniqueID = UniqueID;
                        zOutHeSo.madonhangdoichieu = MaDonHang.Trim();
                        zOutHeSo.madonhang = MaDonHang.Trim();
                        zOutHeSo.heso = s.Trim();

                        _OutputHeso.Add(zOutHeSo);
                    }
                }
                else
                {
                    OutputHeso zOutHeSo = new OutputHeso();
                    zOutHeSo.UniqueID = UniqueID;
                    zOutHeSo.madonhang = MaDonHang.Trim();
                    zOutHeSo.heso = Heso.Trim();

                    _OutputHeso.Add(zOutHeSo);
                }
            }
            catch (Exception Ex)
            {
                Message = Ex.ToString();
            }
            return Message;
        }

        string XuLyListCongNhan(string UniqueID, InputObject InObject, string thoigiandonhang)
        {
            string Message = "";
            try
            {
                float tongthoigianghinangsuat = 0;
                for (int i = 0; i < InObject.ListCongNhan.Count; i++)
                {
                    InputCongNhan zInCongNhan = InObject.ListCongNhan[i];
                    if (zInCongNhan.thoigian != "")
                    {
                        tongthoigianghinangsuat += float.Parse(zInCongNhan.thoigian);
                    }
                }

                foreach (InputCongNhan Cn in InObject.ListCongNhan)
                {
                    InputCongNhan zInCongNhan = Cn;
                    OutputCongNhan zOutCongNhan = new OutputCongNhan();

                    zOutCongNhan.UniqueID = UniqueID;
                    zOutCongNhan.manv = zInCongNhan.manv.Trim();
                    zOutCongNhan.madonhang = zInCongNhan.madonhang.Trim();
                    zOutCongNhan.madonhangdoichieu = zInCongNhan.madonhang.Trim();
                    zOutCongNhan.tennhanvien = zInCongNhan.tennhanvien.Trim();
                    zOutCongNhan.soro = zInCongNhan.soro.Trim();
                    zOutCongNhan.soluong = zInCongNhan.soluong.Trim();
                    zOutCongNhan.thoigian = zInCongNhan.thoigian.Trim();
                    zOutCongNhan.muonnguoi = zInCongNhan.muonnguoi.Trim();
                    zOutCongNhan.anrieng = zInCongNhan.anrieng.Trim();
                    zOutCongNhan.anchung = zInCongNhan.anchung.Trim();

                    //*******************************************************************//
                    if (thoigiandonhang != string.Empty)
                    {
                        //float tongthoigianghinangsuat = InObject.ListCongNhan.Sum(s => float.Parse(s.thoigian));
                        zOutCongNhan.tongthoigianghinangsuat = tongthoigianghinangsuat.ToString();
                        float zthoigian = 0;
                        if (zInCongNhan.thoigian != "")
                        {
                            zthoigian = float.Parse(zInCongNhan.thoigian);
                        }
                        zOutCongNhan.thoigianphanbo = (float.Parse(thoigiandonhang) / tongthoigianghinangsuat * zthoigian).ToString();
                    }
                    //*******************************************************************//
                    _OutputCongNhan.Add(zOutCongNhan);
                }

            }
            catch (Exception Ex)
            {
                Message = Ex.ToString();
            }
            return Message;
        }

        #endregion

        #region // xử lý data đã format //
        private string FilterOutputObject()
        {
            try
            {
                foreach (OutputDonHang DonHang in _OutputDonHang)
                {
                    OutputObject zObject = new OutputObject();
                    zObject.UniqueID = DonHang.UniqueID;
                    zObject.madonhangdoichieu = DonHang.madonhangdoichieu;
                    zObject.madonhang = DonHang.madonhang;
                    zObject.ngaydonhang = DonHang.ngaydonhang;
                    zObject.masanpham = DonHang.masanpham;
                    zObject.tensanpham = DonHang.tensanpham;
                    zObject.manhom = DonHang.manhom;
                    zObject.nhom = DonHang.nhom;
                    zObject.macongdoan = DonHang.macongdoan;
                    zObject.congdoan = DonHang.congdoan;
                    zObject.dongia = DonHang.dongia;
                    zObject.sochungtu = DonHang.sochungtu;
                    zObject.slchungtu = DonHang.slchungtu;
                    zObject.slthucte = DonHang.slthucte;
                    zObject.slhaohut = DonHang.slhaohut;
                    zObject.donvi = DonHang.donvi;
                    zObject.tinhtrang = DonHang.tinhtrang;
                    zObject.tylehoanthanh = DonHang.tylehoanthanh;
                    zObject.ghichu = DonHang.ghichu;
                    zObject.heso = DonHang.heso;
                    zObject.tgphanbo = DonHang.tgphanbo;
                    zObject.tongslthucte = DonHang.slthucte;
                    zObject.tongthoigian = DonHang.tongthoigian;

                    zObject.ListHeSo = _OutputHeso.FindAll(x => x.UniqueID == DonHang.UniqueID);
                    zObject.ListCongNhan = _OutputCongNhan.FindAll(x => x.UniqueID == DonHang.UniqueID);

                    _ListOutputObject.Add(zObject);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            txt_Message.Text += "tổng xử lý đối tường " + _ListOutputObject.Count;
            return string.Empty;
        }
        private string RenameObject()
        {
            try
            {
                int Numer = 0;
                string madoichieu = "";

                foreach (OutputObject DonHang in _ListOutputObject)
                {
                    if (madoichieu == DonHang.madonhangdoichieu)
                    {
                        Numer++;
                    }
                    else
                    {
                        Numer = 1;
                        madoichieu = DonHang.madonhangdoichieu;
                    }

                    //string[] tempthaydoi = DonHang.madonhang.Split('-');
                    //string[] tempdoichieu = DonHang.madonhangdoichieu.Split('-');
                    //Prefix = tempthaydoi[0] + "-" + tempthaydoi[1];
                    //string NewMa = "PM" + Prefix.Substring(2) + "-" + Numer.ToString("0000"); //GetRandomKey(4);             

                    string NewMa = DonHang.madonhangdoichieu + "-" + Numer.ToString("0000");
                    DonHang.madonhangdoichieu = DonHang.madonhangdoichieu;
                    DonHang.madonhang = NewMa;
                    DonHang.ListCongNhan.ForEach(s => s.madonhangdoichieu = DonHang.madonhangdoichieu);
                    DonHang.ListCongNhan.ForEach(s => s.madonhang = NewMa);
                    DonHang.ListHeSo.ForEach(s => s.madonhangdoichieu = DonHang.madonhangdoichieu);
                    DonHang.ListHeSo.ForEach(s => s.madonhang = NewMa);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        private string InitNewOutput()
        {
            try
            {
                _OutputDonHang = new List<OutputDonHang>();
                _OutputCongNhan = new List<OutputCongNhan>();
                _OutputHeso = new List<OutputHeso>();

                foreach (OutputObject Output in _ListOutputObject)
                {
                    OutputDonHang Donhang = new OutputDonHang();
                    Donhang.UniqueID = Output.UniqueID;
                    Donhang.madonhangdoichieu = Output.madonhangdoichieu;
                    Donhang.madonhang = Output.madonhang;
                    Donhang.ngaydonhang = Output.ngaydonhang;
                    Donhang.manhom = Output.manhom;
                    Donhang.nhom = Output.nhom;
                    Donhang.masanpham = Output.masanpham;
                    Donhang.tensanpham = Output.tensanpham;
                    Donhang.macongdoan = Output.macongdoan;
                    Donhang.congdoan = Output.congdoan;
                    Donhang.dongia = Output.dongia;
                    Donhang.sochungtu = Output.sochungtu;
                    Donhang.slchungtu = Output.slchungtu;
                    Donhang.slthucte = Output.slthucte;
                    Donhang.slhaohut = Output.slhaohut;
                    Donhang.donvi = Output.donvi;
                    Donhang.tinhtrang = Output.tinhtrang;
                    Donhang.tylehoanthanh = Output.tylehoanthanh;
                    Donhang.ghichu = Output.ghichu;
                    Donhang.heso = Output.heso;
                    Donhang.tgphanbo = Output.tgphanbo;
                    Donhang.tongthoigian = Output.tongthoigian;
                    Donhang.tongslthucte = Output.tongslthucte;

                    _OutputDonHang.Add(Donhang);
                    _OutputCongNhan.AddRange(Output.ListCongNhan);
                    _OutputHeso.AddRange(Output.ListHeSo);
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion

        private string GetRandomKey(int len)
        {
            int maxSize = len;
            char[] chars = new char[30];
            string a;
            a = "1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data) { result.Append(chars[b % (chars.Length)]); }
            return result.ToString();
        }
    }
}