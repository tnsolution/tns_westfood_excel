﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace WestFoodTools
{
    public static class Data_Access
    {
        public static bool CheckFileStatus(string fileName)
        {
            FileInfo FileInfo = new FileInfo(fileName);
            FileStream streamInput = null;
            try
            {
                streamInput = FileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (streamInput != null)
                {
                    streamInput.Close();
                }
            }
            return false;
        }
        public static DataTable GetSheet(string FilePath)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("sheetName");
            FileInfo fi = new FileInfo(FilePath);
            if (!fi.Exists)
            {
                throw new Exception("File " + FilePath + " Does Not Exists");
            }
            bool zcheck = true;
            zcheck = Data_Access.CheckFileStatus(fi.FullName);
            if (zcheck == false)
            {
                using (ExcelPackage xlPackage = new ExcelPackage(fi))
                {
                    foreach (ExcelWorksheet worksheet in xlPackage.Workbook.Worksheets)
                    {
                        DataRow dr = dt.NewRow();
                        dr["sheetName"] = worksheet.Name;
                        dt.Rows.Add(dr);
                    }
                }
            }

            return dt;
        }
        public static DataTable GetTable(string FilePath, string SheetName)
        {
            DataTable dt = new DataTable();
            FileInfo fi = new FileInfo(FilePath);

            // Check if the file exists
            if (!fi.Exists)
            {
                throw new Exception("File " + FilePath + " Does Not Exists");
            }

            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                // get the first worksheet in the workbook
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[SheetName];// [SheetName];

                // Fetch the WorkSheet size
                ExcelCellAddress startCell = worksheet.Dimension.Start;
                ExcelCellAddress endCell = worksheet.Dimension.End;

                // create all the needed DataColumn
                for (int col = startCell.Column; col <= endCell.Column; col++)
                {
                    dt.Columns.Add(col.ToString());
                }

                // place all the data into DataTable [ + 1 là trừ dòng đầu]
                for (int row = startCell.Row + 1; row <= endCell.Row; row++)
                {
                    DataRow dr = dt.NewRow();
                    int x = 0;
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        dr[x++] = worksheet.Cells[row, col].Value;
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataTable GetTable(string FilePath, string SheetName, int RowStart)
        {
            DataTable dt = new DataTable();
            FileInfo fi = new FileInfo(FilePath);

            // Check if the file exists
            if (!fi.Exists)
            {
                throw new Exception("File " + FilePath + " Does Not Exists");
            }

            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                // get the first worksheet in the workbook
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[SheetName];// [SheetName];

                // Fetch the WorkSheet size
                ExcelCellAddress startCell = worksheet.Dimension.Start;
                ExcelCellAddress endCell = worksheet.Dimension.End;

                // create all the needed DataColumn
                for (int col = startCell.Column; col <= endCell.Column; col++)
                {
                    dt.Columns.Add(col.ToString());
                }

                // place all the data into DataTable [ + 1 là trừ dòng đầu]
                for (int row = startCell.Row + RowStart; row <= endCell.Row; row++)
                {
                    DataRow dr = dt.NewRow();
                    int x = 0;
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        dr[x++] = worksheet.Cells[row, col].Value;
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static string ExportListToExcel(List<OutputDonHang> DonHang, List<OutputCongNhan> CongNhan, List<OutputHeso> PhanBo, string Folder)
        {
            try
            {

                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    newFile.Delete();
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DonHang");
                    worksheet.Cells["A1"].LoadFromCollection(DonHang, true);
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    //Tạo Sheet 2 mới với tên Sheet là DonHang
                    worksheet = package.Workbook.Worksheets.Add("CongNhan");
                    worksheet.Cells["A1"].LoadFromCollection(CongNhan, true);
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    //Tạo Sheet 3 mới với tên Sheet là PhanBo
                    worksheet = package.Workbook.Worksheets.Add("HeSo");
                    worksheet.Cells["A1"].LoadFromCollection(PhanBo, true);
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
    }
}
